export enum NodeType {
  // eslint-disable-next-line no-unused-vars
  START = 'Start',
  // eslint-disable-next-line no-unused-vars
  USER_TASK = 'UserTask',
  // eslint-disable-next-line no-unused-vars
  EXCLUSIVE_GATEWAY = 'ExclusiveGateway',
  // eslint-disable-next-line no-unused-vars
  PARALLEL_GATEWAY = 'ParallelGateway',
  // eslint-disable-next-line no-unused-vars
  INCLUSIVE_GATEWAY = 'InclusiveGateway',
  // eslint-disable-next-line no-unused-vars
  END = 'End'
}
