import { HtmlNodeModel, HtmlNode } from '@logicflow/core'

class StartModel extends HtmlNodeModel {
  properties = {
    name: '结束'
  }

  setAttributes () {
    this.text.editable = false // 禁止节点文本编辑
    // 设置节点宽高和锚点
    const width = 50
    const height = 50
    this.width = width
    this.height = height
    this.anchorsOffset = [
      [width / 2, 0],
      [0, height / 2],
      [-width / 2, 0],
      [0, -height / 2]
    ]
  }
}
class StartNode extends HtmlNode {
  setHtml (rootEl: HTMLElement) {
    const { properties } = this.props.model
    const el = document.createElement('div')
    el.className = 'end'
    const html = `
      <div class="end-container">
         <div class="title">${properties.name}</div>
      </div>

    `
    el.innerHTML = html
    // 需要先把之前渲染的子节点清除掉。
    rootEl.innerHTML = ''
    rootEl.appendChild(el)
  }
}

export default {
  type: 'End',
  view: StartNode,
  model: StartModel
}
