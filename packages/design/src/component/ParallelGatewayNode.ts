import { HtmlNodeModel, HtmlNode } from '@logicflow/core'

class ParallelGatewayModel extends HtmlNodeModel {
  setAttributes () {
    this.text.editable = false // 禁止节点文本编辑
    // 设置节点宽高和锚点
    const width = 70
    const height = 70
    this.width = width
    this.height = height
    this.anchorsOffset = [
      [width / 2, 0],
      [0, height / 2],
      [-width / 2, 0],
      [0, -height / 2]
    ]
  }
}
class ParallelGatewayNode extends HtmlNode {
  setHtml (rootEl: HTMLElement) {
    const el = document.createElement('div')
    el.className = 'parallel-gateway'
    const html = `
     <svg t="1673587628275" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3899" width="40" height="40"><path d="M474 152m8 0l60 0q8 0 8 8l0 704q0 8-8 8l-60 0q-8 0-8-8l0-704q0-8 8-8Z" fill="#000000" p-id="3900"></path><path d="M168 474m8 0l672 0q8 0 8 8l0 60q0 8-8 8l-672 0q-8 0-8-8l0-60q0-8 8-8Z" fill="#000000" p-id="3901"></path></svg>
    `
    el.innerHTML = html
    // 需要先把之前渲染的子节点清除掉。
    rootEl.innerHTML = ''
    rootEl.appendChild(el)
  }
}

export default {
  type: 'ParallelGateway',
  view: ParallelGatewayNode,
  model: ParallelGatewayModel
}
