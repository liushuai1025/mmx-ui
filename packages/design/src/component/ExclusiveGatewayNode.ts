import { HtmlNodeModel, HtmlNode } from '@logicflow/core'

class ExclusiveGatewayModel extends HtmlNodeModel {
  setAttributes () {
    this.text.editable = false // 禁止节点文本编辑
    // 设置节点宽高和锚点
    const width = 70
    const height = 70
    this.width = width
    this.height = height
    this.anchorsOffset = [
      [width / 2, 0],
      [0, height / 2],
      [-width / 2, 0],
      [0, -height / 2]
    ]
  }
}
class ExclusiveGatewayNode extends HtmlNode {
  setHtml (rootEl: HTMLElement) {
    const el = document.createElement('div')
    el.className = 'exclusive-gateway'
    const html = `
     <svg t="1673580622862" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2398" width="30" height="30"><path d="M867.648 951.296 512 595.648l-355.648 355.648c-11.52 11.52-30.272 11.52-41.856 0L72.64 909.44c-11.52-11.52-11.52-30.272 0-41.856L428.352 512 72.64 156.352c-11.52-11.52-11.52-30.272 0-41.856l41.856-41.856c11.52-11.52 30.272-11.52 41.856 0L512 428.288l355.648-355.648c11.52-11.52 30.272-11.52 41.856 0l41.856 41.856c11.52 11.52 11.52 30.272 0 41.856L595.648 512l355.648 355.648c11.52 11.52 11.52 30.272 0 41.856l-41.856 41.856C897.984 962.88 879.168 962.88 867.648 951.296L867.648 951.296z" p-id="2399"></path></svg>
    `
    el.innerHTML = html
    // 需要先把之前渲染的子节点清除掉。
    rootEl.innerHTML = ''
    rootEl.appendChild(el)
  }
}

export default {
  type: 'ExclusiveGateway',
  view: ExclusiveGatewayNode,
  model: ExclusiveGatewayModel
}
