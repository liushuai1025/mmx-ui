import { HtmlNodeModel, HtmlNode } from '@logicflow/core'

class InclusiveGatewayModel extends HtmlNodeModel {
  setAttributes () {
    this.text.editable = false // 禁止节点文本编辑
    // 设置节点宽高和锚点
    const width = 70
    const height = 70
    this.width = width
    this.height = height
    this.anchorsOffset = [
      [width / 2, 0],
      [0, height / 2],
      [-width / 2, 0],
      [0, -height / 2]
    ]
  }
}
class InclusiveGatewayNode extends HtmlNode {
  setHtml (rootEl: HTMLElement) {
    const el = document.createElement('div')
    el.className = 'inclusive-gateway'
    const html = `
     <svg t="1673588368040" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3395" width="30" height="30"><path d="M512 64C264.64 64 64 264.576 64 512c0 247.36 200.64 448 448 448 247.424 0 448-200.64 448-448C960 264.576 759.424 64 512 64zM511.936 896.768C299.52 896.768 127.232 724.48 127.232 512S299.52 127.232 511.936 127.232c212.48 0 384.704 172.224 384.704 384.768S724.416 896.768 511.936 896.768z" fill="#2563EB" p-id="3396"></path></svg>
    `
    el.innerHTML = html
    // 需要先把之前渲染的子节点清除掉。
    rootEl.innerHTML = ''
    rootEl.appendChild(el)
  }
}

export default {
  type: 'InclusiveGateway',
  view: InclusiveGatewayNode,
  model: InclusiveGatewayModel
}
