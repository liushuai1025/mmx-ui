// 折线
import { PolylineEdge, PolylineEdgeModel } from '@logicflow/core'
import { fontSize, themeColor } from '../ts/Common'

class SequenceModel extends PolylineEdgeModel {
  setAttributes () {
    this.offset = 20
  }

  getEdgeStyle () {
    const style = super.getEdgeStyle()
    const { properties } = this
    if (properties.isActived) {
      style.strokeDasharray = '4 4'
    }
    style.stroke = themeColor.value
    return style
  }

  getTextStyle () {
    const style = super.getTextStyle()
    style.color = themeColor.value
    style.fontSize = fontSize.value
    return style
  }

  getOutlineStyle () {
    const style = super.getOutlineStyle()
    // style.stroke = 'red'
    // style.hover.stroke = 'red'
    return style
  }
}

export default {
  type: 'polyline',
  view: PolylineEdge,
  model: SequenceModel
}
