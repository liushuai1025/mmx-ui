import { Component, defineComponent, ref } from 'vue'
import { Config, designProps } from './types'
import LogicFlow from '@logicflow/core'
import '@logicflow/core/dist/style/index.css'
import '@logicflow/extension/lib/style/index.css'
import { componentRegistry, dragPanelData } from './ts/DragPanel'
import { Menu, DndPanel, SelectionSelect } from '@logicflow/extension'
import PropertyPanel from './property-panel/index.vue'
import { themeColor, lf, propertyPanelRef, propertyPanelMap } from './ts/Common'
import { menuData } from './ts/Menu'
import { v4 as uuidv4 } from 'uuid'
import { eventInit } from './ts/Event'
const NAME = 'mmx-design'
export default defineComponent({
  name: NAME,
  props: designProps,
  setup (props, { expose, emit, slots }) {
    const designRef = ref()
    const definitionKey = ref('')
    const init = (config: Config = { themeColor: '#2563EB' }) => {
      if (!config.themeColor) {
        config.themeColor = '#2563EB'
      }
      themeColor.value = config.themeColor
      lf.value = new LogicFlow({
        container: designRef.value,
        grid: true,
        animation: true,
        keyboard: {
          enabled: true
        },
        plugins: [DndPanel, SelectionSelect, Menu]
      })
      // 注册组件
      componentRegistry(lf.value)
      // 注册事件
      eventInit(lf.value)
      lf.value.extension.menu.setMenuConfig(menuData)
      lf.value.extension.dndPanel.setPatternItems(dragPanelData)
    }
    const getData = () => {
      let key = ''
      if (definitionKey.value) {
        key = definitionKey.value
      } else {
        key = uuidv4()
      }

      return {
        definitionKey: key,
        ...lf.value.getGraphData()
      }
    }
    const setData = (dataStr: string) => {
      const jsonObj = JSON.parse(dataStr)
      definitionKey.value = jsonObj.definitionKey
      lf.value.renderRawData(jsonObj)
    }
    const flowRender = (obj: any) => {
      lf.value.render(obj)
    }

    const registerPropertyPanel = (name: string, component: Component) => {
      propertyPanelMap.set(name, component)
    }

    expose({ init, flowRender, getData, setData, registerPropertyPanel })
    return () => (
      <div class="w-full h-full relative border border-solid border-gray-200" >
        <div class={NAME} ref={designRef}>
        </div>
        <PropertyPanel ref={propertyPanelRef}></PropertyPanel>
      </div>

    )
  }
})
