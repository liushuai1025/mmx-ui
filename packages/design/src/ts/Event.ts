import LogicFlow from '@logicflow/core'
import { propertyPanelMap, propertyPanelRef } from './Common'

export const eventInit = (lf: LogicFlow) => {
  const { eventCenter } = lf.graphModel
  // 节点单机事件
  eventCenter.on('node:click', ({ data, e, position }) => {
    const nodeModel = lf.getNodeModelById(data.id)
    const component = propertyPanelMap.get(data.type)
    if (component) {
      propertyPanelRef.value.init(nodeModel.getProperties(), component)
    } else {
      propertyPanelRef.value.close()
    }
    console.log(nodeModel)
    console.log('node:click', data, e, position)
  })
  // 画布单机事件
  eventCenter.on('blank:click', () => {
    propertyPanelRef.value.close()
  })
}
