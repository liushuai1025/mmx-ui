import { ref } from 'vue'

export const themeColor = ref()
export const fontSize = ref(16)
export const lf = ref()
export const propertyPanelRef = ref()
export const propertyPanelMap = new Map()
