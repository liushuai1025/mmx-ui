import LogicFlow from '@logicflow/core/types/LogicFlow'
import UserTask from '../component/UserTaskNode'
import Start from '../component/StartNode'
import Polyline from '../component/PolylineNode'
import ExclusiveGateway from '../component/ExclusiveGatewayNode'
import End from '../component/EndNode'
import ParallelGateway from '../component/ParallelGatewayNode'
import InclusiveGateway from '../component/InclusiveGatewayNode'
import { NodeType } from '../types'

type PatternItem = {
  type?: string;
  text?: string;
  label?: string;
  icon?: string;
  className?: string;
  properties?: object;
  callback?: () => void;
};

export const dragPanelData: PatternItem[] = [
  {
    type: NodeType.START,
    label: '开始节点',
    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAUCAYAAABiS3YzAAAAAXNSR0IArs4c6QAAAh1JREFUOE+10z9oE1EcB/Dv705rUZRW86JTLc1dFAddCnWqdBFC6SJuLu09Ff9QNylF0KFQirqoILbkJYIEpTiICC4uuooOXfTyYrtq7vwTnBq5/Eqsqbk06cVib7t7v9/n/d7v3o+wBQ81M3su/ujesfxrBoTjAI7WxSyC8YoZs4WseN+qnnWoLYszAE2A8QDEj7WKv6klJxxvyCSMMOMyA04hI3LN4BCalP48gwe1Egc26krS+X6MKbgOqrzW6fjdxtg11HK8G0QY0Ur0t9Pm3tGlru3m7pcM3C+o2KP6nN+o7XzpAxmfQHREp2Mf2kGrMYccr79CeGswDrsZ4dbyVlHpVftX1un4lXbBWpwlvSfElXc6s/9WCLWk5xK2ndOqe+2ntIvb0hsGMKGVGGyslLUSTa9XFG6d+bqHOiu+VqLjv6F/2hcqqtbTTVe6EZo3yDztpvcuRB23cd0e8wdg8LxW4mD4+E7xDojKWomr/4xK/ymAklYxGUKTTjHFRFmDcaL+vkVtYJ/1T4E5xztjXYV7tBxCqy+WLE4CNGwyUm5G/IwC+85/6zGDwAUZl3R6X3bdRNU+JKX/jIGATFzLz8U+toITsmQZKL8AI6czYqrl7P+dkOIkgaYBukkBsvmHq3jv6FJnh7FrqEI0TkAKwKxW4kKzjZteeHvs8wBMcxyMkwBEXeICQM8Ds3R7cS5RanWSTU1RVL+3BF0BDFjEFTgHzw4AAAAASUVORK5CYII='
  },
  {
    type: NodeType.USER_TASK,
    label: '用户任务',
    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAAAXNSR0IArs4c6QAAAURJREFUOE+tlDFOw0AQRf/nDFlOsA4FlyAUhIYCpCCEqLKpkLgCEeIOVF4qBFIiQUFDqnAIGmJfgGzOwKC1YisJTmxHdmOtdv7bP7M7Q9T8sWYeMqA2s2tCjgGcVDzkXcCP2DYevC4BBj33BJG2gG8gnisBBZeEnIIcRaG6ou7+HJE7IyFacag+U5juuYPF9aZDfCwFY5HfNoPu7AaUs8iqVioKjBsDsk/wa2LVYRnHiUb4St11fS+IH9Wd/6+uA+Mksqrw8lLdP2BS0zlEG/cdW7VXxmEuUJtph+DAA7yrwLjImyY4nNjG+cY6zjNdcpg6axp3IcALiNsoVPdl0s516IWEDCd2N3PTNNOBgJ2iOq4FrktrK2CZ4q+L2XjL24AzYN7D3ga48LDzW68KdKn1ah8O2TCoc3xVSa8otrDpiwCr+3/MmcK/KDROKQAAAABJRU5ErkJggg==',
    className: 'important-node'
  },
  {
    type: NodeType.EXCLUSIVE_GATEWAY,
    label: '互斥网关',
    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAUCAYAAABiS3YzAAAAAXNSR0IArs4c6QAAAcBJREFUOE+11D9LHEEYBvDnmTMfQMP6BbJ7loJaCIpJkYhF1EL8U2jh7qKmSBCrQBDSaLoEghbqHgraaKd+gDTa+Q9BQe8E29yApI83r+zFO/W8uz30su3O/HjnnWde4j98jDIdX49BsABiPBlYi1Hrw/9l0Vtw3hCtSrBXKVwSvQV/GKLjIrD2X/m6uVK4KOq4egLkN0N5G4K5I1cKP0IdN/0RxPTfzIv45Urtn8Iext3fjUJ1VK4VD9C4q6dEYQqseY/M9QcoOUkG9T9zsOOnP1FUOyQzUw7Oo7ab/kxgXIR9qWXrwPZ0N4EtITZSgTVo+3qdggEBelIJa7tcK7Ko4+ppIYapZDC5VH+Uq6zBu2ozyOwQWBNgWCHWfpao243q8T/U02KIlvuXktsY9/RqCIbwecIaKeyx7es3FPxKJqz8qfOVghgSo/pTyy9Pq1JpiGR7SnpiVG8IR/XUHtVNVNgvloLHt09MilFdSslXodkpvH0xrGWsZhPm+rBUrErklF8UY51nQd3xs3Oaz6OrJ4SYFeJdVV7UXdCz0+m7IV5X5e0/hDknlM4wNs+eUgUVV2+eVjKQi62JnPxPgW8AHDIFJPRSStwAAAAASUVORK5CYII='
  },
  {
    type: NodeType.PARALLEL_GATEWAY,
    label: '并行网关',
    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAUCAYAAABiS3YzAAAAAXNSR0IArs4c6QAAARxJREFUOE+11DFOAzEQBdD/9w7LBYi5ABRUKEhQUNEg0VB5V4SGJhVNakqqFAnJWeAAINGvuEB8Bw8yiZdlWdYWOG49epoZj4fYwmHIVKW5hmAGYlQt8nko3t33ohtwaonDTPASC/+KbsAHSxy9L/LX3dLsx8KdqNLmBuS9pZw40JccC/9AlV7dgphY8syDqjBP1TI/dngM/A3d02YsGcYWOG9mqAoj1TKvY0NwHTjQqzsCI5vxogm67NpoKONPVGkzEeKKmVxWjztv7bHpQvvgNVoYscRBs4cAhj0z+ex7PCjNkALX87rq7WXqMkreU19m8tf3cPI5/YIT/6gaXm+ndH+/BU+FOHVj8+8t1YLT7dOYhdwVE9z8f4E/AJ465hX4oSqGAAAAAElFTkSuQmCC'
  },
  {
    type: NodeType.INCLUSIVE_GATEWAY,
    label: '包含网关',
    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAUCAYAAABiS3YzAAAAAXNSR0IArs4c6QAAAVVJREFUOE+11D9OwzAUBvDvyxkSLkDSC8DAwp8BJGCBAcHCFEeUhYGKAYbOHRg6dQCaE7AwwYgQK0jMSblA3TPYyLQuJaRNBMGr7Z+f/Z4f8Q+DRWYQySNoXIGop13vumi9mZ+JjsCOIpYcjZey8FR0BLYVsfLe9V7nI7lQFs5Fg1Aeg2wp6nUD2iuXhX+gQdg/AdFU5JYBfdF/ILhJ8DaJ3f0y8De0FsqGdtBQwI4BAyG1fcdAyDMAl2nssQgeo37YPydQVw73LGiAbLbNQUXw56YglE1NHNLRB+nN3Jsv5L0DPiex28qiNTG4UNDLvdjbnhbxEBVSK2LRJsVGM60mJ+f9SK5R43HyVrmR1sTgTkE/9WKvnYV9IU8dcDWJ3d2ZkZqNlb+pjaby7Fu48jr9giv+UWN42J2q+/sZuKOJDVM2f+5SGbi6flqmIeetKez8v4E/ABcgCyR7kzesAAAAAElFTkSuQmCC'
  },
  {
    type: NodeType.END,
    label: '结束节点',
    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAUCAYAAABiS3YzAAAAAXNSR0IArs4c6QAAAk5JREFUOE+1lM1rE1EUxc+dNFGLRa2ZGFqpYDJRGnCThXWhWPwTpJuigjNR24W1VlxbdwraoqBWyQRFcCFduRVUUBShIqKgdaYt/fCDzhSxrpo2c2WSmZCkMw4VOqs33HN/79x733uEdfjIi9nWPb0tsrGxTyBKM9AOcBpgnSC8twhPilx4OaW2TPv5WQVNZs1eYj4HYE9AERc1VbzmpamBJmVjgAjX64TvQPyaWIgz+ACAnW6cGYN6XrxcD65AU1nzBDM/cAXEfGXFsh5N3o9/rE5KKsZ5AgZcuEB0ZDwXfVatKUEzp8fCi8W2LwDtLgXZ6tXyO0b8yk9kjYzAGHPipoVQ54Ta/KliyF5I8sJRkDVa4vmUVL9BUjb6iTDslVNyKinGcwCH7XVDCC2f74k/gk5aqsds5WWeK+voraZGO2qcJhVjnIAUgBeaKnYGAd24pBgfAOyz/zVVrMzHdboIoOk/oJUKvaCzzjTX6tQfmlLMxwzusssoNkRSk3e3aEEtSCu/mwsoLJR1/EpTYwdreyobl4gwuJbpS7J5FsQ3nZwLel4cqoHad33DpsY37tVksKKrsbyfW+nkz/0QQk/Lc+ApK2x1TIzE52ug9o9z529XBYYtEob03Hbn2ACJnj8xWil0k2WdAWGvU3q/psZuVBuov/uVNjiiJQDfAMyAIYHQWp3MQE5XxVP1Fa1+pcr9PQ4g8Y9hzRLR1a+56C0vjed72t41v3m5iY4BOMSEDIF3AZgD6DuIR5fCkYczd7b+8tvUExp0nILi6wL9CyFZ3RW6mjXvAAAAAElFTkSuQmCC'
  }

]

export const componentRegistry = (lf: LogicFlow) => {
  lf.register(UserTask)
  lf.register(Start)
  lf.register(Polyline)
  lf.register(ExclusiveGateway)
  lf.register(End)
  lf.register(ParallelGateway)
  lf.register(InclusiveGateway)
}
