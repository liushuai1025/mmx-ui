import { lf } from './Common'

export const menuData = {
  nodeMenu: [
    {
      text: '删除',
      callback (node: any) {
        lf.value.deleteNode(node.id)
      }
    }
  ]
}
