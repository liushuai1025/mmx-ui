import { App } from 'vue'
import Design from './src'

Design.install = (app: App) => {
  app.component(Design.name, Design)
}

export default Design
